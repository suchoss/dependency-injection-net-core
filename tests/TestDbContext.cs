using System.Collections.Generic;
using System.Linq;
using Options.DataContext;

namespace tests
{
    public class TestDbContext : IDbContext
    {

        private List<IDbObject> _list = new List<IDbObject>();

        
        public IEnumerable<T> Get<T>() where T : IDbObject
        {
            return _list.Select(x => (T)x).AsEnumerable();
        }

        public T Get<T>(string id) where T : IDbObject
        {
            
        }

        public T Create<T>(T objToCreate) where T : IDbObject
        {
        }

        public void Update<T>(T objToUpdate) where T : IDbObject
        {
        }

        public void Remove<T>(T objToDelete) where T : IDbObject
        {
        }

        public void Remove<T>(string id) where T : IDbObject
        {
        }

    }
}