using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Options.DataContext;

namespace Options.Models
{
    public class Person : DbObjectBase
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
