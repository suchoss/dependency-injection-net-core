using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Options.DataContext;

namespace Options.Models
{
    public class DbObjectBase : IDbObject
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public virtual string Id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public virtual DateTime DateCreated { get; private set; } = DateTime.Now;
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public virtual DateTime DateModified { get; set; } = DateTime.Now;
        public virtual string CreatedBy { get; set; }
        public virtual string ModifiedBy { get; set; }
    }
}
