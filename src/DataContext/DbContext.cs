using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Options.DataContext
{
    public class DbContext : IDbContext
    {
        private readonly IMongoDatabase _database = null;

        public DbContext(IOptions<DbSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Name);
        }

        private IMongoCollection<T> GetCollection<T>() where T : IDbObject
        {
            return _database.GetCollection<T>(typeof(T).Name);
        }

        public IEnumerable<T> Get<T>() where T : IDbObject
        {
            return GetCollection<T>().Find(obj => true).ToEnumerable();
        }

        public T Get<T>(string id) where T : IDbObject
        {
            return GetCollection<T>().Find(obj => obj.Id == id).FirstOrDefault();
        }

        public T Create<T>(T objToCreate) where T : IDbObject
        {
            GetCollection<T>().InsertOne(objToCreate);
            return objToCreate;
        }

        public void Update<T>(T objToUpdate) where T : IDbObject
        {
            GetCollection<T>().ReplaceOne(obj => obj.Id == objToUpdate.Id, objToUpdate);
        }

        public void Remove<T>(T objToDelete) where T : IDbObject
        {
            GetCollection<T>().DeleteOne(book => book.Id == objToDelete.Id);
        }

        public void Remove<T>(string id) where T : IDbObject
        {
            GetCollection<T>().DeleteOne(book => book.Id == id);
        }

    }
}