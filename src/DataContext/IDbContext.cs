using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Options.DataContext
{
    public interface IDbContext
    {

        IEnumerable<T> Get<T>() where T : IDbObject;
        T Get<T>(string id) where T : IDbObject;
        T Create<T>(T objToCreate) where T : IDbObject;
        void Update<T>(T objToUpdate) where T : IDbObject;
        void Remove<T>(T objToDelete) where T : IDbObject;
        void Remove<T>(string id) where T : IDbObject;
        
    }
}