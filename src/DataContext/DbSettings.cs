﻿namespace Options.DataContext
{
    public class DbSettings
    {
        public string ConnectionString { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}