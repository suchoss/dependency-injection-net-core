using System;

namespace Options.DataContext
{
    public interface IDbObject
    {
        string Id { get; set; }
        DateTime DateCreated { get; }
        DateTime DateModified { get; set; }
        string CreatedBy { get; set; }
        string ModifiedBy { get; set; }
    }
}