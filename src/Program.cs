﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Options.DataContext;
using Options.Services;

namespace Options
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = new HostBuilder()
                .ConfigureHostConfiguration(configHost =>
                {
                    configHost.SetBasePath(Directory.GetCurrentDirectory());
                })
                .ConfigureAppConfiguration((hostContext, configApp) =>
                {
                    //configApp.SetBasePath(Directory.GetCurrentDirectory());
                    configApp.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                    configApp.AddEnvironmentVariables(prefix: "PREFIX_");
                    if (args != null)
                    {
                        configApp.AddCommandLine(args);
                    }
                    configApp.AddJsonFile("appsettings.rabbit.json", optional: true, reloadOnChange: true);
                    configApp.AddJsonFile("appsettings.db.json", optional: true, reloadOnChange: true);
                    //configApp.AddKeyPerFile(hostContext.HostingEnvironment.ContentRootPath + "/secrets", optional: true);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<DbSettings>(hostContext.Configuration.GetSection(nameof(DbSettings)));
                    //services.Configure<DbSettings>(hostContext.Configuration);
                    services.AddScoped<IDbContext,DbContext>();

                    services.Configure<PersonServiceSettings>(hostContext.Configuration.GetSection(nameof(PersonServiceSettings)));
                    //services.Configure<PersonServiceSettings>(hostContext.Configuration);
                    services.AddScoped<PersonService>();

                    services.AddHostedService<LifetimeEventsHostedService>();

                })
                .ConfigureLogging((hostContext, configLogging) =>
                {
                    configLogging.AddConsole();
                    configLogging.AddDebug();
                })
                .UseConsoleLifetime()
                .Build();

            await host.RunAsync();
        }

        
    }
}
