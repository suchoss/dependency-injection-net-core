using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Options.Services;

namespace Options
{

    internal class LifetimeEventsHostedService : IHostedService
    {
        private readonly ILogger _logger;
        private readonly IApplicationLifetime _appLifetime;
        private readonly PersonService _personService;

        public LifetimeEventsHostedService(
            ILogger<LifetimeEventsHostedService> logger,
            IApplicationLifetime appLifetime,
            PersonService personService)
        {
            _logger = logger;
            _appLifetime = appLifetime;
            _personService = personService;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _appLifetime.ApplicationStarted.Register(OnStarted);
            _appLifetime.ApplicationStopping.Register(OnStopping);
            _appLifetime.ApplicationStopped.Register(OnStopped);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        private void OnStarted()
        {
            _logger.LogInformation("OnStarted has been called.");
            Console.WriteLine(_personService.ReadPersonSettings());

            var personCreated = _personService.CreatePerson();
            Console.WriteLine($"{personCreated.Name} is {personCreated.Age} years old.");
            // Perform post-startup activities here

            var personFound = _personService.FindPerson(personCreated.Id);
            Console.WriteLine($"Person found: {personFound.Name}, id:  {personFound.Id}.");

            Console.WriteLine("Program ended.");
        }

        private void OnStopping()
        {
            _logger.LogInformation("OnStopping has been called.");

            // Perform on-stopping activities here
        }

        private void OnStopped()
        {
            _logger.LogInformation("OnStopped has been called.");

            // Perform post-stopped activities here
        }
    }
}