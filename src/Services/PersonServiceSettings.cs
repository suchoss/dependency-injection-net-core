namespace Options.Services
{
    public class PersonServiceSettings
    {
        public string FirstSetting { get; set; }
        public string SecondSetting { get; set; }
        public string ThirdSetting { get; set; }
    }
}