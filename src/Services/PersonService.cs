using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Options.DataContext;
using Options.Models;

namespace Options.Services
{
    public class PersonService
    {
        private readonly PersonServiceSettings _personSettings;
        private readonly IDbContext _db;

        public PersonService(IDbContext dbContext, IOptions<PersonServiceSettings> personSettings)
        {
            _db = dbContext;
            _personSettings = personSettings.Value;
        }

        public string ReadPersonSettings()
        {
            return _personSettings.FirstSetting + "/n"
                + _personSettings.SecondSetting + "/n"
                + _personSettings.ThirdSetting;
        }

        public Person CreatePerson()
        {
            Person person = new Person(){
                Name = "Hildegarda",
                Age = 33
            };

            _db.Create(person);

            return person;
        }

        public Person FindPerson(string id)
        {
            return _db.Get<Person>(id);
        }
    }
}